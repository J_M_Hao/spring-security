# Springboot中使用 Spring Security

[官方文档](https://docs.spring.io/spring-security/reference/index.html)

在`SrpingBoot`项目中引入 `Security`依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

随后启动项目即可，这样就简单集成了Security了。

这个是security进行了简单登陆的实现，官方提供的默认账号是user，密码会在启动命令台里打印，下图中的即是密码

![](img/02.png)

每次启动都会随机生成，也可以在配置文件中进行指定

```yml
spring:
  security:
    user:
      name: admin
      password: 123456
      roles: admin
```

## 内存认证

Spring Security 的`InMemoryUserDetailsManager`实现 [UserDetailsService](https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/user-details-service.html#servlet-authentication-userdetailsservice) 以支持存储在内存中的基于用户名/密码的身份验证。 通过实现接口 `InMemoryUserDetailsManager`提供管理

```java
@Bean
    public UserDetailsService users() {
        UserDetails user = User.builder()
                .username("user")
                .password("{bcrypt}$2a$10$GRLdNijSQMUvl/au9ofL.eDwmoohzzS7.rmNSJZ.0FxO/BTk76klW")
                .roles("USER")
                .build();
        UserDetails admin = User.builder()
                .username("admin")
                .password("{noop}123456")
                .roles("USER", "ADMIN")
                .build();
        return new InMemoryUserDetailsManager(user, admin);
    }
```

## 自定义验证

 [UserDetailsService](https://docs.spring.io/spring-security/site/docs/5.7.2/api/org/springframework/security/core/userdetails/UserDetailsService.html) 用于 [`DaoAuthenticationProvider`](https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/dao-authentication-provider.html#servlet-authentication-daoauthenticationprovider) 检索用户名、密码和其他属性，以使用用户名和密码进行身份验证。Spring Security 提供 [内存](https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/in-memory.html#servlet-authentication-inmemory) 和 [JDBC](https://docs.spring.io/spring-security/reference/servlet/authentication/passwords/jdbc.html#servlet-authentication-jdbc) 实现`UserDetailsService`。

```java
// 定义一个bean 实现 UserDetailsService 接口
@Service
public class UserServiceImpl implements UserDetailsService{
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 自定义实现规则，这里 我们自定义
        Set<GrantedAuthority> roles = new HashSet<>();
        roles.add(new SimpleGrantedAuthority("ADMIN"));
        roles.add(new SimpleGrantedAuthority("USER"));


        User user = new User("admin", "{noop}123456", roles);
        // 返回一个实现了 UserDetails 接口的对象
        return user;
    }
}
```

> 这样的写法仅在`AuthenticationManagerBuilder`未填充且未`AuthenticationProviderBean`定义时使用。

## 授权及其他常见配置

```java
@Autowired
    private UserServiceImpl service;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.authorizeHttpRequests()  // 认证请求, 进行请求的权限配置
                .antMatchers("/**").hasRole("USER") // 拥有ADMIN角色的用户可以访问所有的URL。hasRole(String)：如果当前用户有String表示的角色，则返回True
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated() // 其他请求要认证通过才能访问（登录）
                .and()
                .formLogin()   // 自定义登录表单
                .loginPage("/login") // 登录页面的请求
                .usernameParameter("username") // 表单用户名的 key
                .passwordParameter("password") // 表单密码的 key
                .loginProcessingUrl("/login")  // 登录校验的请求
                .defaultSuccessUrl("/index")  // 验证成功
                .failureUrl("/error")      // 验证失败请求
                .permitAll()    // 所有人都能访问
                .and()
                .logout()
                .invalidateHttpSession(true) // session 过期
                .logoutUrl("/logout")  // 退出登录请求
                .logoutSuccessUrl("/login") // 退出后跳转的请求
                .permitAll()
                .and()
                .userDetailsService(service)  // 鉴权 userService
                .csrf()
                .disable() // csrf 关闭
                .build();
    }
```

### 获取认证的用户信息

```java
@GetMapping("/info")
    public String info(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        // 认证对象
        Object obj = authentication.getPrincipal();

        User user = (User) obj;
        return user.toString();
    }
```

从 Spring Security 3.2 开始，我们可以通过添加注释更直接地解析参数。

```java
@GetMapping("/info2")
public User info2(@AuthenticationPrincipal User user){
    return user;
}
```

> 还可以使用`@CurrentUser`注解，可以从文档中了解



# 在其他模板引擎中如何使用 Security Tags

[Thymeleaf 中如何使用 Security](https://zhuanlan.zhihu.com/p/190250675)

[官方文档](https://www.thymeleaf.org/doc/articles/springsecurity.html)

导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
<dependency>
    <groupId>org.thymeleaf.extras</groupId>
    <artifactId>thymeleaf-extras-springsecurity5</artifactId>
</dependency>
```

在页面头引入命名空间

```html
<html xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
```

即可使用。

`index.html`

```html
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/extras/spring-security">
<head>
    <meta charset="UTF-8">
    <title>Security 标签使用</title>
</head>
<body>
<div sec:authorize="isAuthenticated()">
    This content is only shown to authenticated users.
</div>
<div sec:authorize="hasRole('ROLE_ADMIN')">
    This content is only shown to administrators.
</div>
<div sec:authorize="hasRole('ROLE_USER')">
    This content is only shown to users.
</div>

Logged user: <span sec:authentication="name">Bob</span>
Roles: <span sec:authentication="principal.authorities">[ROLE_USER, ROLE_ADMIN]</span>

</body>
</html>
```


# 使用 Security 实现记住我功能

[RememberMe功能实现](https://www.freesion.com/article/86521346239/)

记住我或持久登录身份验证是指网站能够记住会话之间主体的身份。这通常是通过向浏览器发送 cookie 来完成的，该 cookie 在未来的会话中被检测到并导致自动登录发生。Spring Security 为这些操作的发生提供了必要的钩子，并且有两个具体的 remember-me 实现。一种使用散列来保护基于 cookie 的令牌的安全性，另一种使用数据库或其他持久存储机制来存储生成的令牌。

请注意，这两种实现都需要一个`UserDetailsService`. 如果您使用的身份验证提供程序不使用`UserDetailsService`（例如，LDAP 提供程序），那么除非您`UserDetailsService`的应用程序上下文中还有一个 bean，否则它将无法工作。

**实现:**

1. 修改登录页面添加`记住我`选项

   ```html
   <form action="/login" method="post">
       <input name="username">
       <input type="password" name="password">
       <!--  复选框 name 属性修改为 remember-me -->
       <input type="checkbox" name="remember-me"> 记住我</label>
       <button>登录</button>
   </form>
   ```

2. 在设置中设置`RememberMe`相关配置

   ```java
   http
           ......
           .rememberMe() // 开启记住我选项
       	.tokenValiditySeconds(7 * 24 * 60 * 60) //设置过期时间，单位：秒
       	.rememberMeParameter("remember-me") // 记住我参数，默认 remember-me
           ......
   ```

   启动系统，访问登录页面，输入用户名、密码，勾选 **记住我** 选项，系统正常调跳转到了系统首页。关闭浏览器，直接访问系统首页，此时，仍然可以直接访问，并没有重定向到登录页，无需输入用户名、密码。相同的，重启应用之后，同样可以直接访问系统首页，也无需输入用户名、密码。

​		因为在第一次登录后，在`Cookie`中设置了`remember-me`

![this is a picture of browser cookies view ](img/03-0.png)

再次访问时请求中会携带此`Cookie`

![this is a picture of browser request header view](img/03.png)

### 原理

在`UsernamePasswordAuthenticationFilter`的父类`AbstractAuthenticationProcessingFilter`的`doFilter`方法中在身份认证成功后，会执行 `successfulAuthentication` 方法。此方法执行过程中，便会调用 `RememberMeServices` 接口的 `loginSuccess` 方法。

```java
this.rememberMeServices.loginSuccess(request, response, authResult);
```

`rememberMeService` 的初始值为 **NullRememberMeServices**，通过我们的设置`http.rememberMe()`后会默认设置为`TokenBasedRememberMeServices`对象。

**RememberMeServices** 接口的 `loginSuccess` 便会判断登录页的 **记住我** 复选框是否选中，选中的情况下，才会执行后续逻辑。

```java
// AbstractRememberMeServices.loginSuccess
@Override
	public final void loginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication successfulAuthentication) {
		if (!rememberMeRequested(request, this.parameter)) {
			this.logger.debug("Remember-me login not requested.");
			return;
		}
		onLoginSuccess(request, response, successfulAuthentication);
	}
```

具体的`onLoginSuccess`方法是有具体的实现类实现的, 目前的实现可以通过`TokenBasedRememberMeServices`类的`onLoginSuccess`

方法查看。



> remember-me 令牌仅在指定的时间段内有效，前提是用户名、密码和密钥不更改。值得注意的是，这有一个潜在的安全问题，即在令牌过期之前，任何用户代理都可以使用捕获的记住我令牌。这与摘要式身份验证的问题相同。如果委托人知道令牌已被捕获，他们可以轻松更改密码并立即使所有已发布的记住我令牌无效。


