package com.kfm.springbootsecurity.config;

import com.kfm.springbootsecurity.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity // 启用 Security
public class SecurityConfig {

    @Autowired
    private UserServiceImpl service;

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.authorizeHttpRequests()  // 认证请求, 进行请求的权限配置
                .antMatchers("/**").hasRole("USER") // 拥有ADMIN角色的用户可以访问所有的URL。hasRole(String)：如果当前用户有String表示的角色，则返回True
                .antMatchers("/admin/**").hasRole("ADMIN")
                .anyRequest().authenticated() // 其他请求要认证通过才能访问（登录）
                .and()
                .formLogin()   // 自定义登录表单
                .loginPage("/login") // 登录页面的请求
                .usernameParameter("username") // 表单用户名的 key
                .passwordParameter("password") // 表单密码的 key
                .loginProcessingUrl("/login")  // 登录校验的请求
                .defaultSuccessUrl("/index")  // 验证成功
                .failureUrl("/error")      // 验证失败请求
                .permitAll()    // 所有人都能访问
                .and()
                .logout()
                .invalidateHttpSession(true) // session 过期
                .logoutUrl("/logout")  // 退出登录请求
                .logoutSuccessUrl("/login") // 退出后跳转的请求
                .permitAll()
                .and()
                .userDetailsService(service)  // 鉴权 userService
                .csrf()
                .disable() // csrf 关闭
                .rememberMe() // 开启记住我选项
                .tokenValiditySeconds(7 * 24 * 60 * 60) //设置过期时间，单位：秒
                .rememberMeParameter("remember-me") // 记住我参数，默认 remember-me
                .and()
                .build();
    }


//    @Bean
//    public UserDetailsService users() {
//        UserDetails user = User.builder()
//                .username("user")
//                .password("{bcrypt}$2a$10$GRLdNijSQMUvl/au9ofL.eDwmoohzzS7.rmNSJZ.0FxO/BTk76klW")
//                .roles("USER")
//                .build();
//        UserDetails admin = User.builder()
//                .username("admin")
//                .password("{noop}123456")
//                .roles("USER", "ADMIN")
//                .build();
//        return new InMemoryUserDetailsManager(user, admin);
//    }
}
