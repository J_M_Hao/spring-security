package com.kfm.controller;

import com.kfm.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LoginController {
    @Autowired
    private IUserService service;

    @GetMapping({"/", "/login"})
    public ModelAndView toLogin(){
        return new ModelAndView("login");
    }

    @GetMapping("/error")
    public ModelAndView error(){
        return new ModelAndView("error");
    }

    @GetMapping("/index")
    public ModelAndView index(){
        return new ModelAndView("index");
    }


    @GetMapping("/hello")
    public ModelAndView hello(){
        return new ModelAndView("hello");
    }
}
