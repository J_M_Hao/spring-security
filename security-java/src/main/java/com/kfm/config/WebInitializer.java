package com.kfm.config;//package com.kfm.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 *   配置 DispatcherServlet
 */
public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[0];
    }

    // 配置类
    @Override
    protected Class<?>[] getServletConfigClasses() {
        // 加载配置文件
        return new Class[]{SecurityConfig.class, SpringMVCConfig.class, SpringConfig.class};
    }

    // 映射路径
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
