package com.kfm.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Security Filters 初始化
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
