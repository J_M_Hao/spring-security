package com.kfm.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
public class LoginController {

    @GetMapping({"/", "/login"})
    public ModelAndView toLogin(){
        return new ModelAndView("login");
    }


    @GetMapping("/error")
    public ModelAndView error(){
        return new ModelAndView("error");
    }

    @GetMapping("/index")
    public ModelAndView index(){
        return new ModelAndView("index");
    }


    @RequestMapping("/hello")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ModelAndView hello(){
        return new ModelAndView("hello");
    }

    @RequestMapping("/sys/user/add")
    @PreAuthorize("hasAnyAuthority('sys:user:add')")
    public String add(){
        return "success";
    }


    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public String currentUserName(HttpServletRequest request, Principal principal, Authentication authentication) {
        Principal principal1 = request.getUserPrincipal();
        Authentication authentication1 = SecurityContextHolder.getContext().getAuthentication();
        return principal.getName();
    }

}
