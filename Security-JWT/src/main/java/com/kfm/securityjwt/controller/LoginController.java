package com.kfm.securityjwt.controller;

import com.kfm.securityjwt.service.SysLoginService;
import com.kfm.securityjwt.util.AjaxResult;
import com.kfm.securityjwt.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    private SysLoginService loginService;

    /**
     * 登录方法
     * @return 结果
     */
    @PostMapping("/login")
    public AjaxResult login(String username, String password, String code, String uuid)
    {
        AjaxResult ajax = AjaxResult.success();
        // 生成令牌
        String token = loginService.login(username, password, code,
                uuid);
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }


    @GetMapping("/index")
    public AjaxResult index(){
        return AjaxResult.success("访问成功");
    }
}
