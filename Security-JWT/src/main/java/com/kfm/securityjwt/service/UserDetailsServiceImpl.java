package com.kfm.securityjwt.service;

import com.kfm.securityjwt.entity.LoginUser;
import com.kfm.securityjwt.entity.SysUser;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 此处应该是 从数据库读取信息
        SysUser user = new SysUser();
        user.setUserName("admin");
        user.setPassword("$2a$10$JfDftNYqBdJiQU2qxe6fIeRJP7WtZmo2QtEcOl1/Xa84e0pb.xVJe");

        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(1L);
        loginUser.setUser(user);
        return loginUser;
    }
}
